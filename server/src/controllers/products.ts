import { Request, Response } from "express";
import { Products, PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

interface ProductsDTO {
	id: number;
	name: string;
	quantity: string | null;
	price: number | null;
	stock: number | null;
	discarted: boolean;
}

export const getProducts = async (
	req: Request,
	res: Response
): Promise<Response> => {
	const products: Products[] = await prisma.products.findMany();
	const productsDTO: ProductsDTO[] = products.map((product) => {
		return {
			id: product.ProductID,
			name: product.ProductName,
			quantity: product.QuantityPerUnit,
			price: product.UnitPrice,
			stock: product.UnitsInStock,
			discarted: product.Discontinued,
		};
	});
	return res.status(200).json(productsDTO);
};

export const getProduct = async (
	req: Request,
	res: Response
): Promise<Response> => {
	const { id } = req.params;
	const product: Products | null = await prisma.products.findUnique({
		where: { ProductID: Number(id) },
	});

	if (!product) return res.status(400).json({ success: false });

	const productDTO: ProductsDTO = {
		id: product.ProductID,
		name: product.ProductName,
		quantity: product.QuantityPerUnit,
		price: product.UnitPrice,
		stock: product.UnitsInStock,
		discarted: product.Discontinued,
	};

	return res.status(200).json(productDTO);
};

// export const createEmployee = async (
// 	req: Request,
// 	res: Response
// ): Promise<Response> => {
// 	const { firstName, lastName } = req.body;
// 	const validate: boolean = firstName && lastName;

// 	if (!validate) return res.status(400).json({ success: false });

// 	const employee: Products = await prisma.employees.create({
// 		data: { FirstName: firstName, LastName: lastName },
// 	});
// 	return res.status(200).json({ success: true, ...employee });
// };
