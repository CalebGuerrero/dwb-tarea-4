# Tarea #4

## Back-end

Para poder comenzar a ejecutar el backend, debemos seguir los siguientes pasos dentro de la carpeta `server`. Primero debemos renombrar el archivo `.env.sample` a `.env`, este contendrá nuestro string de conexión a la base de datos

```
# Este string solo funcionará para SQL Server. Cambie todas las palabras en mayúsculas después del '=' para poder realizar la conexión correctamente

DATABASE_URL='sqlserver://HOST:PORT;database=DATABASE_NAME;user=USERNAME;password=PASSWORD;trustServerCertificate=true'
```

Después instalaremos las dependencias necesarias:

```
yarn install
```

Una vez instaladas las dependencias, crearemos las modelos que usaremos con el ORM ([Prisma](https://github.com/prisma/prisma))

```
yarn prisma introspect
```

Como nuestras tablas de SQL Server tienen index repetidos, prisma nos mostrará un error. Por ello hay que comentarizar las líneas indicadas en `prisma./schema.prisma`

```{prisma}
...

82    //@@index([PostalCode], name: "PostalCode")

...

192   //@@index([CompanyName], name: "CompanyName")
193   //@@index([PostalCode], name: "PostalCode")

...
```

> Asegúrese de que las conexiones TCP / IP estén habilitadas a través del _SQL Server Configuration Manager_ para evitar que no se pueda establecer conexión alguna porque la máquina de destino la rechazó.

Y para terminar con los modelos

```
yarn prisma generate
```

Habiendo hecho lo anterior, transpilaremos nuestro código de `TS` a `JS`. Los archivos se generarán en la carpeta `dist`

```
yarn tsc
```

Para finalizar, solo quedaría ejecutar el código para que nuestro servidor empiece a resivir las peticiones

```
yarn start
```

## Front-end

Aquí iniciaremos instalando las dependencias dentro de la carpeta `client`

```
yarn install
```

A diferencia del back-end, solo será necesario iniciar el servidor

```
yarn serve
```
